import React, { Component } from 'react';
import DisplayComponent from './DisplayComponent'

class MapComponent extends Component {

    state = {
        myArray:[{
            id:1,
            name:'bikram',
            email:'bikram@gmail.com',
            phone:988888888
        },
        {
            id:2,
            name:'rakesh',
            email:'rakesh@gmail.com',
            phone:981111111
        },
        {
            id:3,
            name:'sandis',
            email:'sandis@gmail.com',
            phone:987777777
        },
        {
            id:4,
            name:'bhuwan',
            email:'bhuwan@gmail.com',
            phone:982222222
        }
]
    }

    showData() {
        return <DisplayComponent myArray={this.state.myArray} />
    }

    render() {
        console.log('methods', this.state.myArray.findIndex(x=>x.name==="sandis"))
        console.log('methods', this.state.myArray.find(x=>x.name==="bhuwan"))
        console.log('methods', this.state.myArray.filter(x=>x.email==="sandis@gmail.com"))
        
       

       

        return (
            <div>
                <h1>Map Component</h1>
                {this.showData()}
            </div>
        );
    }
}

export default MapComponent;