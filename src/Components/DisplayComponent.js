import React, { Component } from 'react';
import DisplayItem from './DisplayItem';
import Table from './Table';

class DisplayComponent extends Component {
    render() {
        return (
            <div>
            <table style={{width:500}}>
                <thead>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                </thead>
            <tbody>
                {this.props.myArray.map((item) =>{
                     {/* <DisplayItem itemElements={item}/> */}
                   return <Table itemElements={item}  />  
                    
                })}
                </tbody> 
                   </table> 
            </div>
        );
    }
}

export default DisplayComponent;