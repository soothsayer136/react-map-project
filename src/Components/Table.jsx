import React, { Component } from 'react';

class Table extends Component {
    render() {       
        return (
                 <tr>
                   <td> {this.props.itemElements.name}</td>
                   <td> {this.props.itemElements.email}</td>
                   <td> {this.props.itemElements.phone} </td>
                 </tr>
        );
    }
}

export default Table;